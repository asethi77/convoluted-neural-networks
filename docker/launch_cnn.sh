#!/usr/bin/env bash
source ../cnn_env/bin/activate
cd /src
antlr4 -Dlanguage=Python3 calc.g4

python3.5 $1
