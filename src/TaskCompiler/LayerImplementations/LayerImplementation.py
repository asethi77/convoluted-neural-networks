import logging
from TaskCompiler.TaskExceptions import TaskDefinitionException

import tensorflow
from tensorflow.python.ops.nn import conv2d, max_pool
from tensorflow.python.framework.ops import Tensor
from tensorflow.python.layers.core import dense

from TaskCompiler.Implementation import Implementation


class Layers:

    @classmethod
    def construct_layer_implementation(cls, **kwargs):
        pass


class LayerImplementation(Implementation):

    def __init__(self):
        super().__init__()


class ConvLayerImplementation(LayerImplementation):

    def __init__(self):
        super().__init__()
        self['padding'] = "SAME"
        self['stride'] = 1

    def fully_defined(self):
        lookup_variables = [
            'filterSize',
            'numFilters',
            'padding',
            'stride'
        ]

        return self._variables_defined(lookup_variables)

    def compile(self, input_tensor: Tensor = None, n=0):
        logging.debug("Called compile() on ConvLayer() with input %s" % input_tensor)

        if not self.fully_defined():
            raise Exception("Cannot compile because convolution layer is not fully defined\n")

        arg_found, env, arg_val = self.environment.lookup_symbol('filterSize')
        if not arg_found or arg_val.is_placeholder:
            raise TaskDefinitionException("Cannot compile conv layer because filter size is not constrained")
        else:
            filter_size = int(arg_val.value)

        arg_found, env, arg_val = self.environment.lookup_symbol('numFilters')
        if not arg_found or arg_val.is_placeholder:
            raise TaskDefinitionException("Cannot compile conv layer because number of filters is not constrained")
        else:
            num_filters = int(arg_val.value)

        arg_found, env, arg_val = self.environment.lookup_symbol('padding')
        if not arg_found or arg_val.is_placeholder:
            raise TaskDefinitionException("Cannot compile conv layer because padding is not constrained")
        else:
            padding = arg_val.value

        arg_found, env, arg_val = self.environment.lookup_symbol('stride')
        if not arg_found or arg_val.is_placeholder:
            raise TaskDefinitionException("Cannot compile conv layer because stride is not constrained")
        else:
            stride = int(arg_val.value)

        layer_name = "ConvWeightsRandNorm%d" % n
        filter_weights = tensorflow.random_normal((
            filter_size,                    # filter height
            filter_size,                    # filter width
            input_tensor.shape[3].value,    # num input channels
            num_filters,                    # num output channels
        ), name=layer_name)
        n += 1

        filter_biases = tensorflow.Variable(
            tensorflow.random_normal([num_filters]),
            name=("ConvBiasesRandNorm%d" % n)
        )
        n += 1

        layer_name = "Conv2D%d" % n
        conv_layer = conv2d(
            input_tensor,
            filter_weights,
            [1, stride, stride, 1],
            padding,
            name=layer_name
        )
        n += 1

        layer_name = "Conv2DBiases%d" % n
        bias_layer = tensorflow.nn.bias_add(
            conv_layer,
            filter_biases,
            name=layer_name
        )
        n += 1

        layer_name = "Conv2DReLU%d" % n
        activation_layer = tensorflow.nn.relu(
            bias_layer,
            name=layer_name
        )
        n += 1

        return activation_layer, n

    def as_graph_viz(self, cur_graph=None, n=0):
        logging.debug("Constructing graph node for conv layer %d" % n)
        if cur_graph is None:
            return None

        cur_graph.node(str(n), "CONV (%d)" % n)

        return cur_graph, n + 1


class PoolLayerImplementation(LayerImplementation):

    def __init__(self):
        super().__init__()
        self['padding'] = "SAME"
        self['poolWindowSize'] = 1
        self['stride'] = 1

    def fully_defined(self):
        lookup_variables = [
            'poolWindowSize',
            'padding',
            'stride'
        ]

        return self._variables_defined(lookup_variables)

    def compile(self, input_tensor: Tensor = None, n=0):
        logging.debug("Called compile() on PoolLayer() with input %s" % input_tensor)

        if not self.fully_defined():
            raise Exception("Cannot compile because pool layer is not fully defined\n")

        arg_found, env, arg_val = self.environment.lookup_symbol('poolWindowSize')
        if not arg_found or arg_val.is_placeholder:
            raise TaskDefinitionException("Cannot compile pool layer because kernel window size is not constrained")
        else:
            pool_window_size = int(arg_val.value)

        arg_found, env, arg_val = self.environment.lookup_symbol('padding')
        if not arg_found or arg_val.is_placeholder:
            raise TaskDefinitionException("Cannot compile pool layer because padding rule is not constrained")
        else:
            padding = arg_val.value

        arg_found, env, arg_val = self.environment.lookup_symbol('stride')
        if not arg_found or arg_val.is_placeholder:
            raise TaskDefinitionException("Cannot compile pool layer because stride is not constrained")
        else:
            stride = int(arg_val.value)

        layer_name = "MaxPool%d" % n
        output_tensor = max_pool(
            value=input_tensor,
            ksize=[1, pool_window_size, pool_window_size, 1],
            strides=[1, stride, stride, 1],
            padding=padding,
            name=layer_name
        )
        n += 1

        return output_tensor, n

    def as_graph_viz(self, cur_graph=None, n=0):
        logging.debug("Constructing graph node for pool layer %d" % n)
        if cur_graph is None:
            return None

        cur_graph.node(str(n), "POOL (%d)" % n)

        return cur_graph, n + 1


class FullyConnectedLayerImplementation(LayerImplementation):

    def __init__(self):
        super().__init__()

    def fully_defined(self):
        lookup_variables = [
            'numUnits'
        ]

        return self._variables_defined(lookup_variables)

    def compile(self, input_tensor: Tensor = None, n=0):
        logging.debug("Called compile() on FullyConnected() with input %s" % input_tensor)

        if not self.fully_defined():
            raise Exception("Cannot compile because fully-connected layer is not fully defined\n")

        arg_found, env, arg_val = self.environment.lookup_symbol('numUnits')
        if not arg_found or arg_val.is_placeholder:
            raise TaskDefinitionException("Cannot compile fully-connected layer because number of hidden units is "
                                          "not constrained")
        else:
            num_units = int(arg_val.value)

        input_tensor_flat = tensorflow.layers.flatten(input_tensor)
        layer_name = "Dense%d" % n
        output_tensor = dense(
            input_tensor_flat,
            num_units,
            name=layer_name,
            activation=tensorflow.nn.relu
        )
        n += 1

        return output_tensor, n

    def as_graph_viz(self, cur_graph=None, n=0):
        logging.debug("Constructing graph node for fully-connected layer %d" % n)
        if cur_graph is None:
            return None

        cur_graph.node(str(n), "FullyConnected (%d)" % n)

        return cur_graph, n + 1
