# Generated from /home/aakash/PycharmProjects/convoluted-neural-networks/src/grammars/Task.g4 by ANTLR 4.7
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .TaskParser import TaskParser
else:
    from TaskParser import TaskParser

# This class defines a complete listener for a parse tree produced by TaskParser.
class TaskListener(ParseTreeListener):

    # Enter a parse tree produced by TaskParser#root.
    def enterRoot(self, ctx:TaskParser.RootContext):
        pass

    # Exit a parse tree produced by TaskParser#root.
    def exitRoot(self, ctx:TaskParser.RootContext):
        pass


    # Enter a parse tree produced by TaskParser#taskdef.
    def enterTaskdef(self, ctx:TaskParser.TaskdefContext):
        pass

    # Exit a parse tree produced by TaskParser#taskdef.
    def exitTaskdef(self, ctx:TaskParser.TaskdefContext):
        pass


    # Enter a parse tree produced by TaskParser#taskimpl.
    def enterTaskimpl(self, ctx:TaskParser.TaskimplContext):
        pass

    # Exit a parse tree produced by TaskParser#taskimpl.
    def exitTaskimpl(self, ctx:TaskParser.TaskimplContext):
        pass


    # Enter a parse tree produced by TaskParser#sequence.
    def enterSequence(self, ctx:TaskParser.SequenceContext):
        pass

    # Exit a parse tree produced by TaskParser#sequence.
    def exitSequence(self, ctx:TaskParser.SequenceContext):
        pass


    # Enter a parse tree produced by TaskParser#concat.
    def enterConcat(self, ctx:TaskParser.ConcatContext):
        pass

    # Exit a parse tree produced by TaskParser#concat.
    def exitConcat(self, ctx:TaskParser.ConcatContext):
        pass


    # Enter a parse tree produced by TaskParser#taskref.
    def enterTaskref(self, ctx:TaskParser.TaskrefContext):
        pass

    # Exit a parse tree produced by TaskParser#taskref.
    def exitTaskref(self, ctx:TaskParser.TaskrefContext):
        pass


    # Enter a parse tree produced by TaskParser#layerref.
    def enterLayerref(self, ctx:TaskParser.LayerrefContext):
        pass

    # Exit a parse tree produced by TaskParser#layerref.
    def exitLayerref(self, ctx:TaskParser.LayerrefContext):
        pass


    # Enter a parse tree produced by TaskParser#argref.
    def enterArgref(self, ctx:TaskParser.ArgrefContext):
        pass

    # Exit a parse tree produced by TaskParser#argref.
    def exitArgref(self, ctx:TaskParser.ArgrefContext):
        pass


    # Enter a parse tree produced by TaskParser#argdef.
    def enterArgdef(self, ctx:TaskParser.ArgdefContext):
        pass

    # Exit a parse tree produced by TaskParser#argdef.
    def exitArgdef(self, ctx:TaskParser.ArgdefContext):
        pass


    # Enter a parse tree produced by TaskParser#singleArgRef.
    def enterSingleArgRef(self, ctx:TaskParser.SingleArgRefContext):
        pass

    # Exit a parse tree produced by TaskParser#singleArgRef.
    def exitSingleArgRef(self, ctx:TaskParser.SingleArgRefContext):
        pass


    # Enter a parse tree produced by TaskParser#singleArgDef.
    def enterSingleArgDef(self, ctx:TaskParser.SingleArgDefContext):
        pass

    # Exit a parse tree produced by TaskParser#singleArgDef.
    def exitSingleArgDef(self, ctx:TaskParser.SingleArgDefContext):
        pass


    # Enter a parse tree produced by TaskParser#argval.
    def enterArgval(self, ctx:TaskParser.ArgvalContext):
        pass

    # Exit a parse tree produced by TaskParser#argval.
    def exitArgval(self, ctx:TaskParser.ArgvalContext):
        pass


    # Enter a parse tree produced by TaskParser#argname.
    def enterArgname(self, ctx:TaskParser.ArgnameContext):
        pass

    # Exit a parse tree produced by TaskParser#argname.
    def exitArgname(self, ctx:TaskParser.ArgnameContext):
        pass


    # Enter a parse tree produced by TaskParser#taskid.
    def enterTaskid(self, ctx:TaskParser.TaskidContext):
        pass

    # Exit a parse tree produced by TaskParser#taskid.
    def exitTaskid(self, ctx:TaskParser.TaskidContext):
        pass


    # Enter a parse tree produced by TaskParser#layerid.
    def enterLayerid(self, ctx:TaskParser.LayeridContext):
        pass

    # Exit a parse tree produced by TaskParser#layerid.
    def exitLayerid(self, ctx:TaskParser.LayeridContext):
        pass


