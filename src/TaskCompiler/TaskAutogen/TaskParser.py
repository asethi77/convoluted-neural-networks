# Generated from /home/aakash/PycharmProjects/convoluted-neural-networks/src/grammars/Task.g4 by ANTLR 4.7
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys

def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\32")
        buf.write("\u008d\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\3\2\6\2\"\n\2\r\2\16\2#\3\2")
        buf.write("\3\2\3\3\3\3\3\3\3\3\5\3,\n\3\3\3\3\3\3\3\7\3\61\n\3\f")
        buf.write("\3\16\3\64\13\3\3\3\3\3\3\4\3\4\3\4\3\4\5\4<\n\4\3\5\3")
        buf.write("\5\5\5@\n\5\3\5\3\5\3\5\3\5\6\5F\n\5\r\5\16\5G\3\5\3\5")
        buf.write("\3\6\3\6\3\6\5\6O\n\6\3\6\3\6\3\6\6\6T\n\6\r\6\16\6U\3")
        buf.write("\6\3\6\3\7\3\7\3\7\3\7\5\7^\n\7\3\7\3\7\3\7\3\b\3\b\3")
        buf.write("\b\3\b\5\bg\n\b\3\b\3\b\3\b\3\t\3\t\3\t\7\to\n\t\f\t\16")
        buf.write("\tr\13\t\3\n\3\n\3\n\7\nw\n\n\f\n\16\nz\13\n\3\13\3\13")
        buf.write("\3\13\3\13\3\f\3\f\3\f\5\f\u0083\n\f\3\r\3\r\3\16\3\16")
        buf.write("\3\17\3\17\3\20\3\20\3\20\2\2\21\2\4\6\b\n\f\16\20\22")
        buf.write("\24\26\30\32\34\36\2\4\4\2\22\23\27\27\3\2\4\b\2\u008c")
        buf.write("\2!\3\2\2\2\4\'\3\2\2\2\6;\3\2\2\2\b=\3\2\2\2\nK\3\2\2")
        buf.write("\2\fY\3\2\2\2\16b\3\2\2\2\20k\3\2\2\2\22s\3\2\2\2\24{")
        buf.write("\3\2\2\2\26\177\3\2\2\2\30\u0084\3\2\2\2\32\u0086\3\2")
        buf.write("\2\2\34\u0088\3\2\2\2\36\u008a\3\2\2\2 \"\5\4\3\2! \3")
        buf.write("\2\2\2\"#\3\2\2\2#!\3\2\2\2#$\3\2\2\2$%\3\2\2\2%&\7\2")
        buf.write("\2\3&\3\3\2\2\2\'(\7\n\2\2()\5\34\17\2)+\7\16\2\2*,\5")
        buf.write("\22\n\2+*\3\2\2\2+,\3\2\2\2,-\3\2\2\2-.\7\17\2\2.\62\7")
        buf.write("\f\2\2/\61\5\6\4\2\60/\3\2\2\2\61\64\3\2\2\2\62\60\3\2")
        buf.write("\2\2\62\63\3\2\2\2\63\65\3\2\2\2\64\62\3\2\2\2\65\66\7")
        buf.write("\r\2\2\66\5\3\2\2\2\67<\5\f\7\28<\5\16\b\29<\5\b\5\2:")
        buf.write("<\5\n\6\2;\67\3\2\2\2;8\3\2\2\2;9\3\2\2\2;:\3\2\2\2<\7")
        buf.write("\3\2\2\2=?\7\3\2\2>@\7\22\2\2?>\3\2\2\2?@\3\2\2\2@A\3")
        buf.write("\2\2\2AB\7\16\2\2BC\7\17\2\2CE\7\f\2\2DF\5\6\4\2ED\3\2")
        buf.write("\2\2FG\3\2\2\2GE\3\2\2\2GH\3\2\2\2HI\3\2\2\2IJ\7\r\2\2")
        buf.write("J\t\3\2\2\2KL\7\t\2\2LN\7\16\2\2MO\5\20\t\2NM\3\2\2\2")
        buf.write("NO\3\2\2\2OP\3\2\2\2PQ\7\17\2\2QS\7\f\2\2RT\5\6\4\2SR")
        buf.write("\3\2\2\2TU\3\2\2\2US\3\2\2\2UV\3\2\2\2VW\3\2\2\2WX\7\r")
        buf.write("\2\2X\13\3\2\2\2YZ\7\n\2\2Z[\5\34\17\2[]\7\16\2\2\\^\5")
        buf.write("\20\t\2]\\\3\2\2\2]^\3\2\2\2^_\3\2\2\2_`\7\17\2\2`a\7")
        buf.write("\21\2\2a\r\3\2\2\2bc\7\13\2\2cd\5\36\20\2df\7\16\2\2e")
        buf.write("g\5\20\t\2fe\3\2\2\2fg\3\2\2\2gh\3\2\2\2hi\7\17\2\2ij")
        buf.write("\7\21\2\2j\17\3\2\2\2kp\5\24\13\2lm\7\20\2\2mo\5\24\13")
        buf.write("\2nl\3\2\2\2or\3\2\2\2pn\3\2\2\2pq\3\2\2\2q\21\3\2\2\2")
        buf.write("rp\3\2\2\2sx\5\26\f\2tu\7\20\2\2uw\5\26\f\2vt\3\2\2\2")
        buf.write("wz\3\2\2\2xv\3\2\2\2xy\3\2\2\2y\23\3\2\2\2zx\3\2\2\2{")
        buf.write("|\5\32\16\2|}\7\31\2\2}~\5\30\r\2~\25\3\2\2\2\177\u0082")
        buf.write("\5\32\16\2\u0080\u0081\7\31\2\2\u0081\u0083\5\30\r\2\u0082")
        buf.write("\u0080\3\2\2\2\u0082\u0083\3\2\2\2\u0083\27\3\2\2\2\u0084")
        buf.write("\u0085\t\2\2\2\u0085\31\3\2\2\2\u0086\u0087\7\22\2\2\u0087")
        buf.write("\33\3\2\2\2\u0088\u0089\7\22\2\2\u0089\35\3\2\2\2\u008a")
        buf.write("\u008b\t\3\2\2\u008b\37\3\2\2\2\17#+\62;?GNU]fpx\u0082")
        return buf.getvalue()


class TaskParser ( Parser ):

    grammarFileName = "Task.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'Sequence'", "<INVALID>", "'MaxPool'", 
                     "<INVALID>", "<INVALID>", "'Dropout'", "<INVALID>", 
                     "'Task'", "'Layer'", "'{'", "'}'", "'('", "')'", "','", 
                     "';'", "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "'\"'", "<INVALID>", "'.'", "'='" ]

    symbolicNames = [ "<INVALID>", "SEQUENCE", "CONV", "POOL", "FULLY_CONNECTED", 
                      "BATCH_NORM", "DROPOUT", "CONCAT", "TASK_DECL", "LAYER_DECL", 
                      "OPEN_BRACE", "CLOSE_BRACE", "OPEN_PAREN", "CLOSE_PAREN", 
                      "COMMA", "SEMICOLON", "ID", "NUMBER", "INTEGER", "FLOAT", 
                      "QUOTE", "STRING", "PERIOD", "EQUALS", "WS" ]

    RULE_root = 0
    RULE_taskdef = 1
    RULE_taskimpl = 2
    RULE_sequence = 3
    RULE_concat = 4
    RULE_taskref = 5
    RULE_layerref = 6
    RULE_argref = 7
    RULE_argdef = 8
    RULE_singleArgRef = 9
    RULE_singleArgDef = 10
    RULE_argval = 11
    RULE_argname = 12
    RULE_taskid = 13
    RULE_layerid = 14

    ruleNames =  [ "root", "taskdef", "taskimpl", "sequence", "concat", 
                   "taskref", "layerref", "argref", "argdef", "singleArgRef", 
                   "singleArgDef", "argval", "argname", "taskid", "layerid" ]

    EOF = Token.EOF
    SEQUENCE=1
    CONV=2
    POOL=3
    FULLY_CONNECTED=4
    BATCH_NORM=5
    DROPOUT=6
    CONCAT=7
    TASK_DECL=8
    LAYER_DECL=9
    OPEN_BRACE=10
    CLOSE_BRACE=11
    OPEN_PAREN=12
    CLOSE_PAREN=13
    COMMA=14
    SEMICOLON=15
    ID=16
    NUMBER=17
    INTEGER=18
    FLOAT=19
    QUOTE=20
    STRING=21
    PERIOD=22
    EQUALS=23
    WS=24

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class RootContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def EOF(self):
            return self.getToken(TaskParser.EOF, 0)

        def taskdef(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(TaskParser.TaskdefContext)
            else:
                return self.getTypedRuleContext(TaskParser.TaskdefContext,i)


        def getRuleIndex(self):
            return TaskParser.RULE_root

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRoot" ):
                listener.enterRoot(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRoot" ):
                listener.exitRoot(self)




    def root(self):

        localctx = TaskParser.RootContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_root)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 31 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 30
                self.taskdef()
                self.state = 33 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la==TaskParser.TASK_DECL):
                    break

            self.state = 35
            self.match(TaskParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class TaskdefContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def TASK_DECL(self):
            return self.getToken(TaskParser.TASK_DECL, 0)

        def taskid(self):
            return self.getTypedRuleContext(TaskParser.TaskidContext,0)


        def OPEN_PAREN(self):
            return self.getToken(TaskParser.OPEN_PAREN, 0)

        def CLOSE_PAREN(self):
            return self.getToken(TaskParser.CLOSE_PAREN, 0)

        def OPEN_BRACE(self):
            return self.getToken(TaskParser.OPEN_BRACE, 0)

        def CLOSE_BRACE(self):
            return self.getToken(TaskParser.CLOSE_BRACE, 0)

        def argdef(self):
            return self.getTypedRuleContext(TaskParser.ArgdefContext,0)


        def taskimpl(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(TaskParser.TaskimplContext)
            else:
                return self.getTypedRuleContext(TaskParser.TaskimplContext,i)


        def getRuleIndex(self):
            return TaskParser.RULE_taskdef

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTaskdef" ):
                listener.enterTaskdef(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTaskdef" ):
                listener.exitTaskdef(self)




    def taskdef(self):

        localctx = TaskParser.TaskdefContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_taskdef)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 37
            self.match(TaskParser.TASK_DECL)
            self.state = 38
            self.taskid()
            self.state = 39
            self.match(TaskParser.OPEN_PAREN)
            self.state = 41
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==TaskParser.ID:
                self.state = 40
                self.argdef()


            self.state = 43
            self.match(TaskParser.CLOSE_PAREN)
            self.state = 44
            self.match(TaskParser.OPEN_BRACE)
            self.state = 48
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << TaskParser.SEQUENCE) | (1 << TaskParser.CONCAT) | (1 << TaskParser.TASK_DECL) | (1 << TaskParser.LAYER_DECL))) != 0):
                self.state = 45
                self.taskimpl()
                self.state = 50
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 51
            self.match(TaskParser.CLOSE_BRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class TaskimplContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def taskref(self):
            return self.getTypedRuleContext(TaskParser.TaskrefContext,0)


        def layerref(self):
            return self.getTypedRuleContext(TaskParser.LayerrefContext,0)


        def sequence(self):
            return self.getTypedRuleContext(TaskParser.SequenceContext,0)


        def concat(self):
            return self.getTypedRuleContext(TaskParser.ConcatContext,0)


        def getRuleIndex(self):
            return TaskParser.RULE_taskimpl

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTaskimpl" ):
                listener.enterTaskimpl(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTaskimpl" ):
                listener.exitTaskimpl(self)




    def taskimpl(self):

        localctx = TaskParser.TaskimplContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_taskimpl)
        try:
            self.state = 57
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [TaskParser.TASK_DECL]:
                self.enterOuterAlt(localctx, 1)
                self.state = 53
                self.taskref()
                pass
            elif token in [TaskParser.LAYER_DECL]:
                self.enterOuterAlt(localctx, 2)
                self.state = 54
                self.layerref()
                pass
            elif token in [TaskParser.SEQUENCE]:
                self.enterOuterAlt(localctx, 3)
                self.state = 55
                self.sequence()
                pass
            elif token in [TaskParser.CONCAT]:
                self.enterOuterAlt(localctx, 4)
                self.state = 56
                self.concat()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class SequenceContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def SEQUENCE(self):
            return self.getToken(TaskParser.SEQUENCE, 0)

        def OPEN_PAREN(self):
            return self.getToken(TaskParser.OPEN_PAREN, 0)

        def CLOSE_PAREN(self):
            return self.getToken(TaskParser.CLOSE_PAREN, 0)

        def OPEN_BRACE(self):
            return self.getToken(TaskParser.OPEN_BRACE, 0)

        def CLOSE_BRACE(self):
            return self.getToken(TaskParser.CLOSE_BRACE, 0)

        def ID(self):
            return self.getToken(TaskParser.ID, 0)

        def taskimpl(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(TaskParser.TaskimplContext)
            else:
                return self.getTypedRuleContext(TaskParser.TaskimplContext,i)


        def getRuleIndex(self):
            return TaskParser.RULE_sequence

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSequence" ):
                listener.enterSequence(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSequence" ):
                listener.exitSequence(self)




    def sequence(self):

        localctx = TaskParser.SequenceContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_sequence)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 59
            self.match(TaskParser.SEQUENCE)
            self.state = 61
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==TaskParser.ID:
                self.state = 60
                self.match(TaskParser.ID)


            self.state = 63
            self.match(TaskParser.OPEN_PAREN)
            self.state = 64
            self.match(TaskParser.CLOSE_PAREN)
            self.state = 65
            self.match(TaskParser.OPEN_BRACE)
            self.state = 67 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 66
                self.taskimpl()
                self.state = 69 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << TaskParser.SEQUENCE) | (1 << TaskParser.CONCAT) | (1 << TaskParser.TASK_DECL) | (1 << TaskParser.LAYER_DECL))) != 0)):
                    break

            self.state = 71
            self.match(TaskParser.CLOSE_BRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ConcatContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def CONCAT(self):
            return self.getToken(TaskParser.CONCAT, 0)

        def OPEN_PAREN(self):
            return self.getToken(TaskParser.OPEN_PAREN, 0)

        def CLOSE_PAREN(self):
            return self.getToken(TaskParser.CLOSE_PAREN, 0)

        def OPEN_BRACE(self):
            return self.getToken(TaskParser.OPEN_BRACE, 0)

        def CLOSE_BRACE(self):
            return self.getToken(TaskParser.CLOSE_BRACE, 0)

        def argref(self):
            return self.getTypedRuleContext(TaskParser.ArgrefContext,0)


        def taskimpl(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(TaskParser.TaskimplContext)
            else:
                return self.getTypedRuleContext(TaskParser.TaskimplContext,i)


        def getRuleIndex(self):
            return TaskParser.RULE_concat

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterConcat" ):
                listener.enterConcat(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitConcat" ):
                listener.exitConcat(self)




    def concat(self):

        localctx = TaskParser.ConcatContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_concat)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 73
            self.match(TaskParser.CONCAT)
            self.state = 74
            self.match(TaskParser.OPEN_PAREN)
            self.state = 76
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==TaskParser.ID:
                self.state = 75
                self.argref()


            self.state = 78
            self.match(TaskParser.CLOSE_PAREN)
            self.state = 79
            self.match(TaskParser.OPEN_BRACE)
            self.state = 81 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 80
                self.taskimpl()
                self.state = 83 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << TaskParser.SEQUENCE) | (1 << TaskParser.CONCAT) | (1 << TaskParser.TASK_DECL) | (1 << TaskParser.LAYER_DECL))) != 0)):
                    break

            self.state = 85
            self.match(TaskParser.CLOSE_BRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class TaskrefContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def TASK_DECL(self):
            return self.getToken(TaskParser.TASK_DECL, 0)

        def taskid(self):
            return self.getTypedRuleContext(TaskParser.TaskidContext,0)


        def OPEN_PAREN(self):
            return self.getToken(TaskParser.OPEN_PAREN, 0)

        def CLOSE_PAREN(self):
            return self.getToken(TaskParser.CLOSE_PAREN, 0)

        def SEMICOLON(self):
            return self.getToken(TaskParser.SEMICOLON, 0)

        def argref(self):
            return self.getTypedRuleContext(TaskParser.ArgrefContext,0)


        def getRuleIndex(self):
            return TaskParser.RULE_taskref

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTaskref" ):
                listener.enterTaskref(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTaskref" ):
                listener.exitTaskref(self)




    def taskref(self):

        localctx = TaskParser.TaskrefContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_taskref)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 87
            self.match(TaskParser.TASK_DECL)
            self.state = 88
            self.taskid()
            self.state = 89
            self.match(TaskParser.OPEN_PAREN)
            self.state = 91
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==TaskParser.ID:
                self.state = 90
                self.argref()


            self.state = 93
            self.match(TaskParser.CLOSE_PAREN)
            self.state = 94
            self.match(TaskParser.SEMICOLON)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class LayerrefContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LAYER_DECL(self):
            return self.getToken(TaskParser.LAYER_DECL, 0)

        def layerid(self):
            return self.getTypedRuleContext(TaskParser.LayeridContext,0)


        def OPEN_PAREN(self):
            return self.getToken(TaskParser.OPEN_PAREN, 0)

        def CLOSE_PAREN(self):
            return self.getToken(TaskParser.CLOSE_PAREN, 0)

        def SEMICOLON(self):
            return self.getToken(TaskParser.SEMICOLON, 0)

        def argref(self):
            return self.getTypedRuleContext(TaskParser.ArgrefContext,0)


        def getRuleIndex(self):
            return TaskParser.RULE_layerref

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLayerref" ):
                listener.enterLayerref(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLayerref" ):
                listener.exitLayerref(self)




    def layerref(self):

        localctx = TaskParser.LayerrefContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_layerref)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 96
            self.match(TaskParser.LAYER_DECL)
            self.state = 97
            self.layerid()
            self.state = 98
            self.match(TaskParser.OPEN_PAREN)
            self.state = 100
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==TaskParser.ID:
                self.state = 99
                self.argref()


            self.state = 102
            self.match(TaskParser.CLOSE_PAREN)
            self.state = 103
            self.match(TaskParser.SEMICOLON)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ArgrefContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def singleArgRef(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(TaskParser.SingleArgRefContext)
            else:
                return self.getTypedRuleContext(TaskParser.SingleArgRefContext,i)


        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(TaskParser.COMMA)
            else:
                return self.getToken(TaskParser.COMMA, i)

        def getRuleIndex(self):
            return TaskParser.RULE_argref

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterArgref" ):
                listener.enterArgref(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitArgref" ):
                listener.exitArgref(self)




    def argref(self):

        localctx = TaskParser.ArgrefContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_argref)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 105
            self.singleArgRef()
            self.state = 110
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==TaskParser.COMMA:
                self.state = 106
                self.match(TaskParser.COMMA)
                self.state = 107
                self.singleArgRef()
                self.state = 112
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ArgdefContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def singleArgDef(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(TaskParser.SingleArgDefContext)
            else:
                return self.getTypedRuleContext(TaskParser.SingleArgDefContext,i)


        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(TaskParser.COMMA)
            else:
                return self.getToken(TaskParser.COMMA, i)

        def getRuleIndex(self):
            return TaskParser.RULE_argdef

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterArgdef" ):
                listener.enterArgdef(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitArgdef" ):
                listener.exitArgdef(self)




    def argdef(self):

        localctx = TaskParser.ArgdefContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_argdef)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 113
            self.singleArgDef()
            self.state = 118
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==TaskParser.COMMA:
                self.state = 114
                self.match(TaskParser.COMMA)
                self.state = 115
                self.singleArgDef()
                self.state = 120
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class SingleArgRefContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def argname(self):
            return self.getTypedRuleContext(TaskParser.ArgnameContext,0)


        def EQUALS(self):
            return self.getToken(TaskParser.EQUALS, 0)

        def argval(self):
            return self.getTypedRuleContext(TaskParser.ArgvalContext,0)


        def getRuleIndex(self):
            return TaskParser.RULE_singleArgRef

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSingleArgRef" ):
                listener.enterSingleArgRef(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSingleArgRef" ):
                listener.exitSingleArgRef(self)




    def singleArgRef(self):

        localctx = TaskParser.SingleArgRefContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_singleArgRef)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 121
            self.argname()
            self.state = 122
            self.match(TaskParser.EQUALS)
            self.state = 123
            self.argval()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class SingleArgDefContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def argname(self):
            return self.getTypedRuleContext(TaskParser.ArgnameContext,0)


        def EQUALS(self):
            return self.getToken(TaskParser.EQUALS, 0)

        def argval(self):
            return self.getTypedRuleContext(TaskParser.ArgvalContext,0)


        def getRuleIndex(self):
            return TaskParser.RULE_singleArgDef

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSingleArgDef" ):
                listener.enterSingleArgDef(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSingleArgDef" ):
                listener.exitSingleArgDef(self)




    def singleArgDef(self):

        localctx = TaskParser.SingleArgDefContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_singleArgDef)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 125
            self.argname()
            self.state = 128
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==TaskParser.EQUALS:
                self.state = 126
                self.match(TaskParser.EQUALS)
                self.state = 127
                self.argval()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ArgvalContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NUMBER(self):
            return self.getToken(TaskParser.NUMBER, 0)

        def STRING(self):
            return self.getToken(TaskParser.STRING, 0)

        def ID(self):
            return self.getToken(TaskParser.ID, 0)

        def getRuleIndex(self):
            return TaskParser.RULE_argval

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterArgval" ):
                listener.enterArgval(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitArgval" ):
                listener.exitArgval(self)




    def argval(self):

        localctx = TaskParser.ArgvalContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_argval)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 130
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << TaskParser.ID) | (1 << TaskParser.NUMBER) | (1 << TaskParser.STRING))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ArgnameContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(TaskParser.ID, 0)

        def getRuleIndex(self):
            return TaskParser.RULE_argname

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterArgname" ):
                listener.enterArgname(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitArgname" ):
                listener.exitArgname(self)




    def argname(self):

        localctx = TaskParser.ArgnameContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_argname)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 132
            self.match(TaskParser.ID)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class TaskidContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(TaskParser.ID, 0)

        def getRuleIndex(self):
            return TaskParser.RULE_taskid

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTaskid" ):
                listener.enterTaskid(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTaskid" ):
                listener.exitTaskid(self)




    def taskid(self):

        localctx = TaskParser.TaskidContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_taskid)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 134
            self.match(TaskParser.ID)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class LayeridContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def CONV(self):
            return self.getToken(TaskParser.CONV, 0)

        def POOL(self):
            return self.getToken(TaskParser.POOL, 0)

        def FULLY_CONNECTED(self):
            return self.getToken(TaskParser.FULLY_CONNECTED, 0)

        def BATCH_NORM(self):
            return self.getToken(TaskParser.BATCH_NORM, 0)

        def DROPOUT(self):
            return self.getToken(TaskParser.DROPOUT, 0)

        def getRuleIndex(self):
            return TaskParser.RULE_layerid

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLayerid" ):
                listener.enterLayerid(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLayerid" ):
                listener.exitLayerid(self)




    def layerid(self):

        localctx = TaskParser.LayeridContext(self, self._ctx, self.state)
        self.enterRule(localctx, 28, self.RULE_layerid)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 136
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << TaskParser.CONV) | (1 << TaskParser.POOL) | (1 << TaskParser.FULLY_CONNECTED) | (1 << TaskParser.BATCH_NORM) | (1 << TaskParser.DROPOUT))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





