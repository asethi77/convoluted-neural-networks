# Generated from /home/aakash/PycharmProjects/convoluted-neural-networks/src/grammars/Task.g4 by ANTLR 4.7
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\32")
        buf.write("\u00dc\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write("\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write("\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23")
        buf.write("\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30")
        buf.write("\4\31\t\31\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\3\3\3")
        buf.write("\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\5")
        buf.write("\3L\n\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3")
        buf.write("\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5")
        buf.write("\3\5\3\5\5\5i\n\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6")
        buf.write("\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3")
        buf.write("\6\3\6\3\6\3\6\3\6\5\6\u0086\n\6\3\7\3\7\3\7\3\7\3\7\3")
        buf.write("\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b")
        buf.write("\3\b\3\b\3\b\3\b\3\b\3\b\5\b\u00a1\n\b\3\t\3\t\3\t\3\t")
        buf.write("\3\t\3\n\3\n\3\n\3\n\3\n\3\n\3\13\3\13\3\f\3\f\3\r\3\r")
        buf.write("\3\16\3\16\3\17\3\17\3\20\3\20\3\21\6\21\u00bb\n\21\r")
        buf.write("\21\16\21\u00bc\3\22\3\22\5\22\u00c1\n\22\3\23\6\23\u00c4")
        buf.write("\n\23\r\23\16\23\u00c5\3\24\3\24\3\24\3\24\3\25\3\25\3")
        buf.write("\26\3\26\3\26\3\26\3\27\3\27\3\30\3\30\3\31\6\31\u00d7")
        buf.write("\n\31\r\31\16\31\u00d8\3\31\3\31\2\2\32\3\3\5\4\7\5\t")
        buf.write("\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20")
        buf.write("\37\21!\22#\23%\24\'\25)\26+\27-\30/\31\61\32\3\2\5\4")
        buf.write("\2C\\c|\3\2\62;\5\2\13\f\17\17\"\"\2\u00e3\2\3\3\2\2\2")
        buf.write("\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r")
        buf.write("\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3")
        buf.write("\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2")
        buf.write("\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'")
        buf.write("\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2")
        buf.write("\61\3\2\2\2\3\63\3\2\2\2\5K\3\2\2\2\7M\3\2\2\2\th\3\2")
        buf.write("\2\2\13\u0085\3\2\2\2\r\u0087\3\2\2\2\17\u00a0\3\2\2\2")
        buf.write("\21\u00a2\3\2\2\2\23\u00a7\3\2\2\2\25\u00ad\3\2\2\2\27")
        buf.write("\u00af\3\2\2\2\31\u00b1\3\2\2\2\33\u00b3\3\2\2\2\35\u00b5")
        buf.write("\3\2\2\2\37\u00b7\3\2\2\2!\u00ba\3\2\2\2#\u00c0\3\2\2")
        buf.write("\2%\u00c3\3\2\2\2\'\u00c7\3\2\2\2)\u00cb\3\2\2\2+\u00cd")
        buf.write("\3\2\2\2-\u00d1\3\2\2\2/\u00d3\3\2\2\2\61\u00d6\3\2\2")
        buf.write("\2\63\64\7U\2\2\64\65\7g\2\2\65\66\7s\2\2\66\67\7w\2\2")
        buf.write("\678\7g\2\289\7p\2\29:\7e\2\2:;\7g\2\2;\4\3\2\2\2<=\7")
        buf.write("E\2\2=>\7q\2\2>?\7p\2\2?@\7x\2\2@A\7q\2\2AB\7n\2\2BC\7")
        buf.write("w\2\2CD\7v\2\2DE\7k\2\2EF\7q\2\2FL\7p\2\2GH\7E\2\2HI\7")
        buf.write("q\2\2IJ\7p\2\2JL\7x\2\2K<\3\2\2\2KG\3\2\2\2L\6\3\2\2\2")
        buf.write("MN\7O\2\2NO\7c\2\2OP\7z\2\2PQ\7R\2\2QR\7q\2\2RS\7q\2\2")
        buf.write("ST\7n\2\2T\b\3\2\2\2UV\7H\2\2VW\7w\2\2WX\7n\2\2XY\7n\2")
        buf.write("\2YZ\7{\2\2Z[\7E\2\2[\\\7q\2\2\\]\7p\2\2]^\7p\2\2^_\7")
        buf.write("g\2\2_`\7e\2\2`a\7v\2\2ab\7g\2\2bi\7f\2\2cd\7F\2\2de\7")
        buf.write("g\2\2ef\7p\2\2fg\7u\2\2gi\7g\2\2hU\3\2\2\2hc\3\2\2\2i")
        buf.write("\n\3\2\2\2jk\7D\2\2kl\7c\2\2lm\7v\2\2mn\7e\2\2no\7j\2")
        buf.write("\2op\7P\2\2pq\7q\2\2qr\7t\2\2rs\7o\2\2st\7c\2\2tu\7n\2")
        buf.write("\2uv\7k\2\2vw\7|\2\2wx\7c\2\2xy\7v\2\2yz\7k\2\2z{\7q\2")
        buf.write("\2{\u0086\7p\2\2|}\7D\2\2}~\7c\2\2~\177\7v\2\2\177\u0080")
        buf.write("\7e\2\2\u0080\u0081\7j\2\2\u0081\u0082\7P\2\2\u0082\u0083")
        buf.write("\7q\2\2\u0083\u0084\7t\2\2\u0084\u0086\7o\2\2\u0085j\3")
        buf.write("\2\2\2\u0085|\3\2\2\2\u0086\f\3\2\2\2\u0087\u0088\7F\2")
        buf.write("\2\u0088\u0089\7t\2\2\u0089\u008a\7q\2\2\u008a\u008b\7")
        buf.write("r\2\2\u008b\u008c\7q\2\2\u008c\u008d\7w\2\2\u008d\u008e")
        buf.write("\7v\2\2\u008e\16\3\2\2\2\u008f\u0090\7E\2\2\u0090\u0091")
        buf.write("\7q\2\2\u0091\u0092\7p\2\2\u0092\u0093\7e\2\2\u0093\u0094")
        buf.write("\7c\2\2\u0094\u0095\7v\2\2\u0095\u0096\7g\2\2\u0096\u0097")
        buf.write("\7p\2\2\u0097\u0098\7c\2\2\u0098\u0099\7v\2\2\u0099\u00a1")
        buf.write("\7g\2\2\u009a\u009b\7E\2\2\u009b\u009c\7q\2\2\u009c\u009d")
        buf.write("\7p\2\2\u009d\u009e\7e\2\2\u009e\u009f\7c\2\2\u009f\u00a1")
        buf.write("\7v\2\2\u00a0\u008f\3\2\2\2\u00a0\u009a\3\2\2\2\u00a1")
        buf.write("\20\3\2\2\2\u00a2\u00a3\7V\2\2\u00a3\u00a4\7c\2\2\u00a4")
        buf.write("\u00a5\7u\2\2\u00a5\u00a6\7m\2\2\u00a6\22\3\2\2\2\u00a7")
        buf.write("\u00a8\7N\2\2\u00a8\u00a9\7c\2\2\u00a9\u00aa\7{\2\2\u00aa")
        buf.write("\u00ab\7g\2\2\u00ab\u00ac\7t\2\2\u00ac\24\3\2\2\2\u00ad")
        buf.write("\u00ae\7}\2\2\u00ae\26\3\2\2\2\u00af\u00b0\7\177\2\2\u00b0")
        buf.write("\30\3\2\2\2\u00b1\u00b2\7*\2\2\u00b2\32\3\2\2\2\u00b3")
        buf.write("\u00b4\7+\2\2\u00b4\34\3\2\2\2\u00b5\u00b6\7.\2\2\u00b6")
        buf.write("\36\3\2\2\2\u00b7\u00b8\7=\2\2\u00b8 \3\2\2\2\u00b9\u00bb")
        buf.write("\t\2\2\2\u00ba\u00b9\3\2\2\2\u00bb\u00bc\3\2\2\2\u00bc")
        buf.write("\u00ba\3\2\2\2\u00bc\u00bd\3\2\2\2\u00bd\"\3\2\2\2\u00be")
        buf.write("\u00c1\5%\23\2\u00bf\u00c1\5\'\24\2\u00c0\u00be\3\2\2")
        buf.write("\2\u00c0\u00bf\3\2\2\2\u00c1$\3\2\2\2\u00c2\u00c4\t\3")
        buf.write("\2\2\u00c3\u00c2\3\2\2\2\u00c4\u00c5\3\2\2\2\u00c5\u00c3")
        buf.write("\3\2\2\2\u00c5\u00c6\3\2\2\2\u00c6&\3\2\2\2\u00c7\u00c8")
        buf.write("\5%\23\2\u00c8\u00c9\5-\27\2\u00c9\u00ca\5%\23\2\u00ca")
        buf.write("(\3\2\2\2\u00cb\u00cc\7$\2\2\u00cc*\3\2\2\2\u00cd\u00ce")
        buf.write("\5)\25\2\u00ce\u00cf\5!\21\2\u00cf\u00d0\5)\25\2\u00d0")
        buf.write(",\3\2\2\2\u00d1\u00d2\7\60\2\2\u00d2.\3\2\2\2\u00d3\u00d4")
        buf.write("\7?\2\2\u00d4\60\3\2\2\2\u00d5\u00d7\t\4\2\2\u00d6\u00d5")
        buf.write("\3\2\2\2\u00d7\u00d8\3\2\2\2\u00d8\u00d6\3\2\2\2\u00d8")
        buf.write("\u00d9\3\2\2\2\u00d9\u00da\3\2\2\2\u00da\u00db\b\31\2")
        buf.write("\2\u00db\62\3\2\2\2\13\2Kh\u0085\u00a0\u00bc\u00c0\u00c5")
        buf.write("\u00d8\3\2\3\2")
        return buf.getvalue()


class TaskLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    SEQUENCE = 1
    CONV = 2
    POOL = 3
    FULLY_CONNECTED = 4
    BATCH_NORM = 5
    DROPOUT = 6
    CONCAT = 7
    TASK_DECL = 8
    LAYER_DECL = 9
    OPEN_BRACE = 10
    CLOSE_BRACE = 11
    OPEN_PAREN = 12
    CLOSE_PAREN = 13
    COMMA = 14
    SEMICOLON = 15
    ID = 16
    NUMBER = 17
    INTEGER = 18
    FLOAT = 19
    QUOTE = 20
    STRING = 21
    PERIOD = 22
    EQUALS = 23
    WS = 24

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'Sequence'", "'MaxPool'", "'Dropout'", "'Task'", "'Layer'", 
            "'{'", "'}'", "'('", "')'", "','", "';'", "'\"'", "'.'", "'='" ]

    symbolicNames = [ "<INVALID>",
            "SEQUENCE", "CONV", "POOL", "FULLY_CONNECTED", "BATCH_NORM", 
            "DROPOUT", "CONCAT", "TASK_DECL", "LAYER_DECL", "OPEN_BRACE", 
            "CLOSE_BRACE", "OPEN_PAREN", "CLOSE_PAREN", "COMMA", "SEMICOLON", 
            "ID", "NUMBER", "INTEGER", "FLOAT", "QUOTE", "STRING", "PERIOD", 
            "EQUALS", "WS" ]

    ruleNames = [ "SEQUENCE", "CONV", "POOL", "FULLY_CONNECTED", "BATCH_NORM", 
                  "DROPOUT", "CONCAT", "TASK_DECL", "LAYER_DECL", "OPEN_BRACE", 
                  "CLOSE_BRACE", "OPEN_PAREN", "CLOSE_PAREN", "COMMA", "SEMICOLON", 
                  "ID", "NUMBER", "INTEGER", "FLOAT", "QUOTE", "STRING", 
                  "PERIOD", "EQUALS", "WS" ]

    grammarFileName = "Task.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


