from collections import defaultdict
from typing import Any, Tuple, DefaultDict


class EnvironmentPlaceHolder:
    placeholder_instance = None

    def __init__(self):
        if EnvironmentPlaceHolder.placeholder_instance is None:
            EnvironmentPlaceHolder.placeholder_instance = self

    @classmethod
    def instance(cls):
        return cls.placeholder_instance


class EnvironmentVariable:

    def __init__(self, value, is_placeholder: bool = False):
        self.is_placeholder = is_placeholder
        if is_placeholder:
            self.value = EnvironmentPlaceHolder()
        else:
            # prevent EnvironmentVariables from containing EnvironmentVariables
            if isinstance(value, EnvironmentVariable):
                self.value = value.value
            else:
                self.value = value


class Environment:
    def __init__(self, parent_environment=None):
        self.symbol_table = defaultdict(lambda: None)   # type: DefaultDict[str, EnvironmentVariable]
        self.parent_environment = parent_environment    # type: Environment

    def add_symbols(self, **kwargs) -> None:
        if kwargs is not None:
            for key, value in kwargs.items():
                if isinstance(value, EnvironmentVariable):
                    if key in self.symbol_table:
                        self.symbol_table[key].value = value.value
                    else:
                        self.symbol_table[key] = value
                else:
                    if key in self.symbol_table:
                        self.symbol_table[key].value = value
                    else:
                        self.symbol_table[key] = EnvironmentVariable(value)


    def lookup_symbol(self, symbol_name) -> Tuple[bool, Any, EnvironmentVariable]:
        cur_env = self
        # We introduce this in case for whatever reason the value of the symbol is None. However, this should
        # not actually happen given our language definition.
        symbol_defined = False
        while cur_env is not None:
            if symbol_name in cur_env.symbol_table:
                symbol_defined = True
                return symbol_defined, cur_env, cur_env.symbol_table[symbol_name]
            cur_env = cur_env.parent_environment

        return symbol_defined, None, EnvironmentVariable(None, False)
