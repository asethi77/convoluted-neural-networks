from antlr4.error.ErrorListener import ErrorListener

from TaskCompiler.TaskExceptions import TaskParserException


class TaskCompilerErrorListener(ErrorListener):

    def __init__(self):
        super(TaskCompilerErrorListener, self).__init__()

    def syntaxError(self, recognizer, offendingSymbol, line, column, msg, e):
        raise TaskParserException((
                                      "Detected syntax error at line %d column %d while compiling task.\n",
                                      "\tSymbol that raised error: %s\n",
                                      "\tOriginal parser error message: %s\n"
                                  ) % (line, column, offendingSymbol, msg))

    def reportAmbiguity(self, recognizer, dfa, startIndex, stopIndex, exact, ambigAlts, configs):
        raise TaskParserException((
                                      "Detected ambiguous task definition between input indices %d and %d.\n"
                                  ) % (startIndex, stopIndex))

    def reportAttemptingFullContext(self, recognizer, dfa, startIndex, stopIndex, conflictingAlts, configs):
        raise TaskParserException((
                                      "Detected error attempting full context between input indices %d and %d.\n"
                                  ) % (startIndex, stopIndex))

    def reportContextSensitivity(self, recognizer, dfa, startIndex, stopIndex, prediction, configs):
        raise TaskParserException((
                                      "Detected context-sensitive error between input indices %d and %d\n"
                                  ) % (startIndex, stopIndex))
