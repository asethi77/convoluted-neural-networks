from antlr4 import *

from .TaskAutogen.TaskParser import TaskParser as TaskParser
from .TaskAutogen.TaskLexer import TaskLexer as TaskLexer
from .TaskGeneratorParseTreeListener import TaskGeneratorParseTreeListener
from .TaskCompilerErrorListener import TaskCompilerErrorListener


class TaskCompiler:

    def __init__(self):
        pass

    @staticmethod
    def compile_string(input_str, listener: TaskGeneratorParseTreeListener = None):
        input_stream = InputStream(input_str)
        if listener is None:
            listener = TaskGeneratorParseTreeListener()
        return TaskCompiler._compile(input_stream, listener)

    @staticmethod
    def compile_file(file_name, listener: TaskGeneratorParseTreeListener = None):
        input_stream = FileStream(file_name)
        if listener is None:
            listener = TaskGeneratorParseTreeListener()
        return TaskCompiler._compile(input_stream, listener)

    @classmethod
    def _compile(cls, input_stream: InputStream, listener: TaskGeneratorParseTreeListener = None):
        lexer = TaskLexer(input_stream)
        stream = CommonTokenStream(lexer)
        parser = TaskParser(stream)

        tree = parser.root()
        walker = ParseTreeWalker()
        parser.addErrorListener(TaskCompilerErrorListener())
        if listener is None:
            listener = TaskGeneratorParseTreeListener()
            walker.walk(listener, tree)
        else:
            walker.walk(listener, tree)

        return listener.compiled_tasks
