class TaskDefinitionException(Exception):

    def __init__(self, exception_str):
        super(TaskDefinitionException, self).__init__(exception_str)


class TaskParserException(Exception):

    def __init__(self, exception_str):
        super(TaskParserException, self).__init__(exception_str)
