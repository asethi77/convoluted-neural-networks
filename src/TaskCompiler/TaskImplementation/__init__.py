from .TaskImplNode import TaskImplNode
from .TaskImplementation import TaskImplementation, SequenceImplementation
from .ConcatImplementation import ConcatImplementation
from .TaskImplNode import TaskImplNode, TaskImplNodeType
from TaskCompiler.Environment import Environment, EnvironmentPlaceHolder, EnvironmentVariable

placeholder = EnvironmentPlaceHolder()
