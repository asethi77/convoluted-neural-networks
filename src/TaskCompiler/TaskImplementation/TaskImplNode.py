from enum import Enum

from TaskCompiler.TaskExceptions import TaskParserException, TaskDefinitionException
from TaskCompiler.LayerImplementations import LayerImplementation
from TaskCompiler.Implementation import Implementation


class TaskImplNodeType(Enum):
    TASK = 1
    LAYER = 2
    TASK_PLACEHOLDER = 3
    SEQUENCE = 4
    CONCAT = 5


class TaskImplNode:

    def __init__(self, name, node_type: TaskImplNodeType, instance):
        # We cannot have this import at the module level, otherwise we'd have circular imports.
        from TaskCompiler.TaskImplementation import TaskImplementation, SequenceImplementation, ConcatImplementation

        self.task = None                            # type: TaskImplementation
        self.layer = None                           # type: LayerImplementation
        self.sequence = None                        # type: SequenceImplementation
        self.concat = None                          # type: ConcatImplementation
        self.name = name
        self.type = node_type

        if node_type == TaskImplNodeType.TASK:
            self.task = instance                    # type: TaskImplementation
        elif node_type == TaskImplNodeType.LAYER:
            self.layer = instance
        elif node_type == TaskImplNodeType.TASK_PLACEHOLDER:
            self.task = None
        elif node_type == TaskImplNodeType.SEQUENCE:
            self.sequence = instance
        elif node_type == TaskImplNodeType.CONCAT:
            self.concat = instance
        else:
            raise TaskParserException("Encountered internal error identifing type inside task definition")

    def implementation(self) -> Implementation:
        if self.type == TaskImplNodeType.TASK:
            return self.task

        elif self.type == TaskImplNodeType.LAYER:
            return self.layer

        elif self.type == TaskImplNodeType.TASK_PLACEHOLDER:
            return None

        elif self.type == TaskImplNodeType.SEQUENCE:
            return self.sequence

        elif self.type == TaskImplNodeType.CONCAT:
            return self.concat
