import graphviz
import logging
import tensorflow

from typing import List

from .TaskImplNode import TaskImplNode, TaskImplNodeType
from TaskCompiler.Implementation import Implementation
from TaskCompiler.TaskExceptions import TaskDefinitionException


class ConcatImplementation(Implementation):

    def __init__(self, parent_task=None):
        super().__init__()
        self.parent_task = parent_task              # type: TaskImplNode
        self.task_references = []                   # type: List[TaskImplNode]
        self.dataflow_graph = []                    # type: List[TaskImplNode]
        self["axis"] = 3

    def fully_defined(self):
        lookup_variables = [
            'axis'
        ]

        return self._variables_defined(lookup_variables)

    def compile(self, input_tensor=None, n=0):
        logging.debug("Called compile() on concat block with input %s" % input_tensor)
        if input_tensor is None:
            raise TaskDefinitionException("Cannot compile sequence implementation because input data format is not "
                                          "fully constrained")

        if not self.fully_defined():
            raise Exception("Cannot compile because concat is not fully defined\n")

        arg_found, env, arg_val = self.environment.lookup_symbol('axis')
        if not arg_found or arg_val.is_placeholder:
            raise TaskDefinitionException("Cannot compile because concat axis is not constrained")
        else:
            concat_axis = int(arg_val.value)

        output_tensor = None
        next_n = n
        compiled_outputs = []
        for node in self.dataflow_graph:
            output_tensor, next_n = node.implementation().compile(input_tensor, next_n)
            compiled_outputs.append((output_tensor, next_n - 1))

        tensors_to_concat = [out[0] for out in compiled_outputs]
        output_tensor = tensorflow.concat(
            tensors_to_concat,
            concat_axis,
            name="Concat%d" % next_n
        )
        next_n += 1

        return output_tensor, next_n

    def as_graph_viz(self, cur_graph=None, n=0):
        logging.debug("Constructing graph for  %s (%d)" % ("CONCAT", n))
        graph = cur_graph
        if graph is None:
            raise Exception("Invalid concat graph")
        this_node_id = str(n)
        graph.node(this_node_id, "CONCAT")
        n += 1

        compiled_outputs = []

        for node in self.dataflow_graph:            # type: TaskImplNode
            new_node_id = str(n)
            new_graph, n = node.implementation().as_graph_viz(graph, n)
            compiled_outputs.append((new_graph, n - 1))
            if node.type == TaskImplNodeType.TASK:
                graph.subgraph(new_graph)
            graph.edge(this_node_id, new_node_id)

        graph.node(str(n), "Concat_Merge")
        n += 1
        for node, node_id in compiled_outputs:
            print(str(node_id))
            print(str(n))
            graph.edge(str(node_id), str(n - 1))

        return graph, n