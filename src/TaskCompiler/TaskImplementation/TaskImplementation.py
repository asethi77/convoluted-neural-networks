import graphviz
import logging

from typing import List

from .TaskImplNode import TaskImplNode, TaskImplNodeType
from TaskCompiler.Implementation import Implementation
from TaskCompiler.TaskExceptions import TaskDefinitionException


class TaskImplementation(Implementation):

    def __init__(self, task_name, parent_task=None):
        super().__init__()
        self.task_name = task_name                  # type: str
        self.parent_task = parent_task              # type: TaskImplNode
        self.task_references = []                   # type: List[TaskImplNode]
        self.dataflow_graph = []                    # type: List[TaskImplNode]
        self.arg_list = ["input"]                   # type: List[str]

    def fully_defined(self):
        return True

    def compile(self, input_tensor=None, n=0):
        _input = input_tensor
        if _input is None:
            found, env, _input = self.environment.lookup_symbol("input")
            if not found:
                raise TaskDefinitionException("Cannot compile sequence implementation because input data format is not " 
                                              "fully constrained")
            else:
                if _input.is_placeholder:
                    raise TaskDefinitionException("Cannot compile sequence implementation because input data format is not "
                                                  "fully constrained")
                else:
                    _input = _input.value

        logging.debug("Called compile() on task with input %s" % _input)
        output_tensor = None
        next_n = n
        for node in self.dataflow_graph:
            output_tensor, next_n = node.implementation().compile(_input, next_n)

        if len(self.dataflow_graph) <= 1:
            return output_tensor, next_n

    def as_graph_viz(self, cur_graph=None, n=0):
        logging.debug("Constructing graph for task %s (%d)" % (self.task_name, n))

        if cur_graph is not None:
            graph_name = "cluster_%d_%s" % (n, self.task_name)
        else:
            graph_name = self.task_name
        graph = graphviz.Digraph(name=graph_name)

        this_node_id = str(n)
        graph.node(this_node_id, self.task_name)
        n += 1

        for node in self.dataflow_graph:            # type: TaskImplNode
            new_node_id = str(n)
            new_graph, n = node.implementation().as_graph_viz(graph, n)
            if node.type == TaskImplNodeType.TASK:
                graph.subgraph(new_graph)
            graph.edge(this_node_id, new_node_id)

        return graph, n


class SequenceImplementation(Implementation):

    def __init__(self, parent_task=None):
        super().__init__()
        self.parent_task = parent_task              # type: TaskImplNode
        self.task_references = []                   # type: List[TaskImplNode]
        self.dataflow_graph = []                    # type: List[TaskImplNode]

    def fully_defined(self):
        for node in self.dataflow_graph:
            if not node.implementation().fully_defined():
                return False
        return True

    def compile(self, input_tensor=None, n=0):
        _input = input_tensor
        if _input is None:
            found, env, _input = self.environment.lookup_symbol("input")
            if not found:
                raise TaskDefinitionException("Cannot compile sequence implementation because input data format is not " 
                                              "fully constrained")
            else:
                if _input.is_placeholder:
                    raise TaskDefinitionException("Cannot compile sequence implementation because input data format is not "
                                                  "fully constrained")
                else:
                    _input = _input.value

        logging.debug("Called compile() on sequence with input %s" % _input)
        output_tensor = None
        next_input = _input
        next_n = n
        for node in self.dataflow_graph:
            output_tensor, next_n = node.implementation().compile(next_input, next_n)
            next_input = output_tensor

        return output_tensor, next_n

    def as_graph_viz(self, cur_graph=None, n=0):
        logging.debug("Constructing subgraph for sequence")
        if cur_graph is None:
            raise TaskDefinitionException("sequence cannot be independent of a task implementation")
        logging.debug("Sequence n start is %d" % n)

        this_node_id = str(n)
        cur_graph.node(this_node_id, "Sequence (%s)" % this_node_id)
        n += 1

        for node in self.dataflow_graph:
            new_node_id = n
            new_graph, n = node.implementation().as_graph_viz(cur_graph, n)
            if node.type == TaskImplNodeType.TASK:
                cur_graph.subgraph(new_graph)
            cur_graph.edge(str(new_node_id - 1), str(new_node_id))

        return cur_graph, n
