import copy
import logging
from typing import Dict

from antlr4 import *

from .TaskAutogen.TaskLexer import TaskLexer
from .TaskAutogen.TaskParser import TaskParser
from .TaskAutogen.TaskListener import TaskListener
from .TaskExceptions import TaskParserException, TaskDefinitionException

from .LayerImplementations import ConvLayerImplementation, PoolLayerImplementation, FullyConnectedLayerImplementation
from .TaskImplementation import TaskImplementation, SequenceImplementation, \
    ConcatImplementation, TaskImplNode, TaskImplNodeType, Environment, EnvironmentVariable


class TaskGeneratorParseTreeListener(TaskListener):

    def __init__(self):
        TaskListener.__init__(self)
        self.compiled_tasks = {}                    # type: Dict[str, TaskImplNode]
        self.cur_task = None                        # type: TaskImplNode
        self.cur_context = None                     # type: TaskImplNode
        self.cur_environment = None                 # type: Environment
        self.lexer = TaskLexer()

    def visitErrorNode(self, node: ErrorNode):
        layer_token_type_symbolic = node.symbol.type
        # layer_token_type_symbolic = layer_token.getChild(0).symbol.type
        layer_token_type_string = self.lexer.symbolicNames[layer_token_type_symbolic]
        print(layer_token_type_string)
        raise TaskParserException((
            "Unable to parse symbol %s at line %d column %d"
        ) % (node.symbol.text, node.symbol.line, node.symbol.column))

    def enterTaskdef(self, ctx: TaskParser.TaskdefContext):
        task_name = ctx.getChild(1).getText()
        logging.debug("Entering task %s" % (task_name))
        if task_name in self.compiled_tasks:
            raise TaskDefinitionException("Multiple definitions of task %s" % task_name)
        else:
            self.cur_task = TaskImplNode(
                task_name,
                TaskImplNodeType.TASK,
                TaskImplementation(
                    task_name,
                    self.cur_task.implementation() if self.cur_task is not None else None
                )
            )
            self.cur_context = self.cur_task
            self.cur_environment = self.cur_context.implementation().environment

        # Note that at this point, we do not yet add the task def to the list of compiled
        # tasks. This is because we may run into an ErrorNode deeper in the parse tree,
        # and we should not register this as a compiled task if we end up throwing
        # an exception in the middle of parsing a task.

    def exitTaskdef(self, ctx: TaskParser.TaskdefContext):
        if self.cur_task.implementation().parent_task is not None:
            raise TaskDefinitionException("Unexpected nesting of task definitions")

        task_name = ctx.getChild(1).getText()
        if task_name != self.cur_task.implementation().task_name:
            raise TaskParserException("Attempting to exit task definition for undefined task")
        self.compiled_tasks[task_name] = self.cur_task

        self.cur_task = self.cur_context = self.cur_task.implementation().parent_task
        logging.debug("Exited task %s" % task_name)

    def enterTaskref(self, ctx: TaskParser.TaskrefContext):
        task_name = ctx.getChild(1).getText()
        logging.debug("Task %s contains reference to task %s" % (
            self.cur_task.implementation().task_name, task_name
        ))

        # TODO: The current environment implementation assumes this never happens. Fix this or acknowledge the issue.
        if task_name not in self.compiled_tasks:
            task_ref_implementation = TaskImplNode(
                task_name,
                TaskImplNodeType.TASK_PLACEHOLDER,
                None
            )
        else:
            task_ref_implementation = TaskImplNode(
                task_name,
                TaskImplNodeType.TASK,
                copy.deepcopy(self.compiled_tasks[task_name].implementation())
            )
            parent_environment = self.cur_environment
            self.cur_environment = task_ref_implementation.implementation().environment
            self.cur_environment.parent_environment = parent_environment
        self.cur_context.implementation().task_references.append(task_ref_implementation)
        self.cur_context.implementation().dataflow_graph.append(task_ref_implementation)

    def exitTaskref(self, ctx: TaskParser.TaskrefContext):
        self.cur_environment = self.cur_environment.parent_environment

    def enterLayerref(self, ctx: TaskParser.LayerrefContext):
        layer_token = ctx.getChild(1)
        layer_token_type_symbolic = layer_token.getChild(0).symbol.type
        layer_token_type_string = self.lexer.symbolicNames[layer_token_type_symbolic]
        logging.debug("Entered layer %s" % layer_token_type_string)

        layer_ref_implementation = None
        if layer_token_type_string == "CONV":
            layer_ref_implementation = ConvLayerImplementation()
        elif layer_token_type_string == "POOL":
            layer_ref_implementation = PoolLayerImplementation()
        elif layer_token_type_string == "FULLY_CONNECTED":
            layer_ref_implementation = FullyConnectedLayerImplementation()
        if layer_ref_implementation is None:
            raise TaskDefinitionException("Unimplemented type of layer referenced in task definition")

        layer_impl = TaskImplNode(
            layer_token_type_string,
            TaskImplNodeType.LAYER,
            layer_ref_implementation
        )
        self.cur_context.implementation().dataflow_graph.append(layer_impl)
        parent_environment = self.cur_environment
        self.cur_environment = layer_ref_implementation.environment
        self.cur_environment.parent_environment = parent_environment

    def exitLayerref(self, ctx: TaskParser.LayerrefContext):
        self.cur_environment = self.cur_environment.parent_environment

    def enterSequence(self, ctx: TaskParser.SequenceContext):
        sequence_implementation = SequenceImplementation(self.cur_context)

        sequence_node = TaskImplNode(
            TaskImplNodeType.SEQUENCE.name,
            TaskImplNodeType.SEQUENCE,
            sequence_implementation
        )

        self.cur_context = sequence_node

    def exitSequence(self, ctx: TaskParser.SequenceContext):
        cur_sequence = self.cur_context.implementation()
        cur_sequence.parent_task.implementation().dataflow_graph.append(self.cur_context)
        self.cur_context = cur_sequence.parent_task

    def enterSingleArgDef(self, ctx: TaskParser.SingleArgDefContext):
        # An argument definition defines an argument for a task we are defining, i.e. it defines
        # what parameters are available for an end-user to inject into a task/model. Default values for parameters
        # can be specified, but this is not required.

        # Argument definitions include at least the symbol name, but optionally include a default value for said symbol.
        # We must parse the argdef to determine which is the case, and populate the environment as appropriate.
        argdef_text = ctx.getText()
        argdef_keyword_value = argdef_text.split('=')
        argdef_keyword = argdef_keyword_value[0]
        argdef_value = EnvironmentVariable(None, is_placeholder=True)
        if len(argdef_keyword_value) == 2:
            argdef_value = EnvironmentVariable(argdef_keyword_value[1], is_placeholder=False)

        arg_found, cur_env, arg_val = self.cur_environment.lookup_symbol(argdef_keyword)
        if arg_found:
            raise TaskDefinitionException(
                "Argument %s to context %s has already been defined" % (
                    argdef_keyword,
                    self.cur_context.name
                )
            )

        arg = {
            argdef_keyword: argdef_value
        }

        logging.debug("Got definition for argument %s" % arg)
        self.cur_environment.add_symbols(**arg)
        self.cur_task.implementation().arg_list.append(argdef_keyword)

    def exitSingleArgDef(self, ctx: TaskParser.SingleArgDefContext):
        # self.cur_environment = self.cur_environment.parent_environment
        pass

    def enterSingleArgRef(self, ctx: TaskParser.SingleArgRefContext):
        # An argument reference is a reference to an argument for a previously-
        # defined task or layer implementation.
        argref_text = ctx.getText()
        argref_keyword_value = argref_text.split('=')
        if len(argref_keyword_value) != 2:
            raise TaskDefinitionException(
                "Invalid argument definition %s at line %d column %d" % (
                    argref_text,
                    ctx.start.getLine(),
                    ctx.start.getCharPositionInLine()
                )
            )

        argref_keyword = argref_keyword_value[0]
        argref_value = argref_keyword_value[1]
        if '"' in argref_value:
            argref_value = argref_value[1:-1]

        # We do this lookup from the parent environment because it is possible that the parent task definition has
        # defined an argument with the same name as the one that is referenced, and that the task/layer reference
        # is attempting to use this argument as an rvalue to its own argument with the same name. If we were to
        # perform this search from the current environment, we would enter a circular reference and end up with
        # undefined behavior.
        arg_found, cur_env, arg_val = self.cur_environment.parent_environment.lookup_symbol(argref_value)
        if arg_found:
            arg = {
                argref_keyword: arg_val
            }
        else:
            arg = {
                argref_keyword: EnvironmentVariable(argref_value)
            }

        self.cur_environment.add_symbols(**arg)

    def exitSingleArgRef(self, ctx: TaskParser.SingleArgRefContext):
        pass

    def enterConcat(self, ctx:TaskParser.ConcatContext):
        sequence_implementation = ConcatImplementation(self.cur_context)

        concat_node = TaskImplNode(
            TaskImplNodeType.CONCAT.name,
            TaskImplNodeType.CONCAT,
            sequence_implementation
        )

        self.cur_context = concat_node

    def exitConcat(self, ctx:TaskParser.ConcatContext):
        cur_sequence = self.cur_context.implementation()
        cur_sequence.parent_task.implementation().dataflow_graph.append(self.cur_context)
        self.cur_context = cur_sequence.parent_task
