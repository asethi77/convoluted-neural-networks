from TaskCompiler.Environment import Environment, EnvironmentVariable
from tensorflow.python.framework.ops import Tensor
from typing import Optional
import graphviz


class Implementation:

    def __init__(self):
        self.environment = Environment()        # type: Environment
        self.arg_list = []                      # type: list{str}

    def fully_defined(self) -> bool:
        return False

    def _variables_defined(self, variable_list=[]) -> bool:
        for variable_name in variable_list:
            arg_defined, env_found, arg_val = self.environment.lookup_symbol(variable_name)
            if not arg_defined or env_found != self.environment:
                return False

        return True

    def compile(self, input_tensor: Tensor = None, n=0) -> Optional[Tensor]:
        return None

    def as_graph_viz(self, cur_graph: graphviz.Digraph = None, n: int = 0) -> Optional[graphviz.Digraph]:
        return None

    def __getitem__(self, key: str) -> Optional[EnvironmentVariable]:
        arg_defined, env, arg_val = self.environment.lookup_symbol(key)
        if arg_defined:
            return arg_val
        else:
            return None

    def __setitem__(self, key: str, value) -> None:
        arg_defined, env, arg_val = self.environment.lookup_symbol(key)
        if arg_defined:
            if isinstance(value, EnvironmentVariable):
                arg_val.value = value.value
            else:
                arg_val.value = value
            if arg_val.is_placeholder:
                arg_val.is_placeholder = False

        else:
            arg = {
                key: value
            }
            self.environment.add_symbols(**arg)
