grammar Task;


root
    : taskdef+ EOF
    ;

taskdef
    : TASK_DECL taskid OPEN_PAREN argdef? CLOSE_PAREN OPEN_BRACE taskimpl* CLOSE_BRACE
    ;

taskimpl
    : taskref
    | layerref
    | sequence
    | concat
    ;

sequence
    : SEQUENCE ID? OPEN_PAREN CLOSE_PAREN OPEN_BRACE taskimpl+ CLOSE_BRACE
    ;

concat
    : CONCAT OPEN_PAREN argref? CLOSE_PAREN OPEN_BRACE taskimpl+ CLOSE_BRACE
    ;

taskref
    : TASK_DECL taskid OPEN_PAREN argref? CLOSE_PAREN SEMICOLON
    ;

layerref
    : LAYER_DECL layerid OPEN_PAREN argref? CLOSE_PAREN SEMICOLON
    ;

argref
    : singleArgRef (COMMA singleArgRef)*
    ;

argdef
    : singleArgDef (COMMA singleArgDef)*
    ;

singleArgRef
    : argname EQUALS argval
    ;

singleArgDef
    : argname (EQUALS argval)?
    ;

argval
    : NUMBER
    | STRING
    | ID
    ;

argname
    : ID
    ;

taskid
    : ID
    ;

layerid
    : CONV
    | POOL
    | FULLY_CONNECTED
    | BATCH_NORM
    | DROPOUT
    ;

SEQUENCE
    : 'Sequence'
    ;

CONV
    : 'Convolution'
    | 'Conv'
    ;

POOL
    : 'MaxPool'
    ;

FULLY_CONNECTED
    : 'FullyConnected'
    | 'Dense'
    ;

BATCH_NORM
    : 'BatchNormalization'
    | 'BatchNorm'
    ;

DROPOUT
    : 'Dropout'
    ;

CONCAT
    : 'Concatenate'
    | 'Concat'
    ;

TASK_DECL
    : 'Task'
    ;

LAYER_DECL
    : 'Layer'
    ;

OPEN_BRACE
    : '{'
    ;

CLOSE_BRACE
    : '}'
    ;

OPEN_PAREN
    : '('
    ;

CLOSE_PAREN
    : ')'
    ;

COMMA
    : ','
    ;

SEMICOLON
    : ';'
    ;

ID
    : [a-zA-Z]+
    ;

NUMBER
    : INTEGER
    | FLOAT
    ;

INTEGER
    : [0-9]+
    ;

FLOAT
    : INTEGER PERIOD INTEGER
    ;

QUOTE
    : '"'
    ;

STRING
    : QUOTE ID QUOTE
    ;

PERIOD
    : '.'
    ;

EQUALS
    : '='
    ;

WS
    : [ \t\n\r]+ -> channel(HIDDEN)
    ;