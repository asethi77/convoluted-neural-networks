# Convoluted Neural Networks, Simplified.
## Aakash Sethi and Vadim Eksarevskiy

We are designing a domain-specific language (DSL) intended to minimize the effort of selecting and testing different deep learning network architectures in order to get something done.

[Link to pre-proposal](https://docs.google.com/a/uw.edu/document/d/1G4XmVYjh43W_yk74fFcTWqIwQtgABSfKd-j7w0DlOzU/edit?usp=sharing)

[Link to proposal](https://docs.google.com/a/uw.edu/document/d/1lBeh3-CHCJLNbvQHv73QJzG6vnCu0L9M-9kRbaSvkk8/edit?usp=sharing)

[Link to design document](https://docs.google.com/a/uw.edu/document/d/1zuPLR06iXyqIproa2XY3ZeHOsCdLMDum1uLRgvTRorM/edit?usp=sharing)

[Link to project report](https://docs.google.com/a/uw.edu/document/d/1rthwSwVi3db6s7iotCG34yj1FJpYZwTtA6XH9vcG29A/edit?usp=sharing)

[Link to poster presentation](https://docs.google.com/a/uw.edu/presentation/d/1ZyGV6Xjny17EHLkKS3a0J26zbwsJGjLJm1fkpStYiNk/edit?usp=sharing)


Language Tutorial:

```
In general, our language allows you to specifiy and easily chain different tasks, sequences, and layers in order to easily and effectively create CNN models. 
A Task is an overarching unit of the language used to encapsulate models and parts of a model (similar to a typical programming function. 
It can be definedas follows:

Task nameOfTask(optional params) {
   # different layers inside
}

A Layer is the basic unit from which the tasks can be built. The syntax for defining a layer is as follows:
   Layer Conv(arguements);

Currently, we support the following layers with the following parameters:
   Conv(stride, filterSize, numFilter, padding);
   MaxPool(stride, filterSize, padding)
   Dense(numUnits)  # also known as fully-connected

Layers can either be cahined in a direct 1:1 input to output relation using Sequence() or multiple layers can be stiched together into a single output using Concat().
Here is example usage of both:
   Sequence() {
      # all the layers inside
   }
   
   Concat(axis along which to concatenate the results) {
      # all the layers inside
   }
   
Finally, with all of those constructs, here is what an example model could look like. For a full example including how to fully train and run a dataset 
using this model, please look into the examples folder.


		Task convTask() {                                                               # create a new task to act as a helper
           Layer Conv(stride=1, filterSize=5, numFilters=32, padding="SAME");           # this task consists of just one convolutional layer
        }
        
		
        Task mainTask() {                                                               # creates a main task (the one that you want to be run)
           Sequence() {                                                                 # Starts a direct sequence, where all of the layers following it have chained inputs/outputs
               Task convTask();                                                         # uses the previous sub task (shown here as an example of syntax)
               Layer MaxPool(stride=2, filterSize=2, padding="SAME");                   # create a max pooling layer
               Layer Conv(stride=1, filterSize=5, numFilters=64, padding="SAME");
               Layer MaxPool(stride=2, filterSize=2, padding="SAME");
               Layer Dense(numUnits=1024);
               Layer Dense(numUnits=10);                                                # the final resulting tensor at the end is implicitly outputed
           }
        }
    )
```