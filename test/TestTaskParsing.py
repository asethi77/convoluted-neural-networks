import unittest

from TaskCompiler import *


# @unittest.skip("Only skipping these tests because I know they work right now")
class TestTaskBasicParsing(unittest.TestCase):
    """
    Unit test suite to validate barebones task definitions.
    """
    # @unittest.skip("Because")
    def test_empty_task_def(self):
        input_string = (
            "Task a() {\n"
            "\n"
            "}\n"
        )

        print("test_empty_task_def: verify parsing empty task")
        print(input_string)
        compiler = TaskCompiler()
        compiler.compile_string(input_string)

    # @unittest.skip("Because")
    def test_empty_task_def_with_args(self):
        input_string = (
            "Task a(argOne, argTwo=3) {\n"
            "\n"
            "}\n"
        )

        print("test_empty_task_def_with_args: verify parsing empty task with dummy arguments")
        print(input_string)
        compiler = TaskCompiler()
        compiler.compile_string(input_string)

    @unittest.skip("Not implementing all of these layers")
    def test_task_def_layers_only(self):
        layer_str_options = [
            "   Layer Convolution(filterSize=5, numFilters=16);\n",
            "   Layer Conv(filterSize=filterSize);\n",
            "   Layer MaxPool(filterSize=4);\n",
            "   Layer BatchNormalization();\n",
            "   Layer Dropout(probability=0.2);\n",
            "   Layer Conv(filterSize=1, numFilters=5);\n",
            "   Layer BatchNorm();\n",
            "   Layer FullyConnected(numNodes=1024);\n"
        ]

        print("test_task_def_layers_only: verify parsing single-level task with all possible layer types")

        for layer_str in layer_str_options:
            input_string = (
                "Task a(filterSize) {\n" +
                layer_str +
                "}\n"
            )
            print(input_string)
            compiler = TaskCompiler()
            compiler.compile_string(input_string)

    # @unittest.skip("Because")
    def test_task_def_sequence(self):
        input_string = (
            "Task seqTask(argOne, argTwo=3) {\n"
            "   Sequence() {\n"
            "       Layer Conv(filterSize=3, numFilters=8);\n"
            "       Layer MaxPool(filterSize=5);\n"
            "   }\n"
            "}\n"
        )

        print("test_task_def_sequence: verify parsing sequence of layers")
        print(input_string)
        compiler = TaskCompiler()
        compiler.compile_string(input_string)

    def test_concat_task_def(self):
        input_string = (
            "Task concatTask(argOne, argTwo=3) {\n"
            "   Concat() {\n"
            "       Sequence() {\n"
            "           Layer Conv(filterSize=1);\n"
            "           Layer Conv(filterSize=3);\n"
            "       }\n"
            "       Sequence() {\n"
            "           Layer Conv(filterSize=1);\n"
            "           Layer Conv(filterSize=5);\n"
            "       }\n"
            "       Sequence() {\n"
            "           Layer MaxPool(poolWindowSize=3);\n"
            "           Layer Conv(filterSize=1);\n"
            "       }\n"
            "       Layer Conv(filterSize=1);\n"
            "   }\n"
            "}\n"
        )

        print("test_concat_task_def: verify parsing concatenated sequences of layers")
        print(input_string)
        compiler = TaskCompiler()
        compiled_tasks = compiler.compile_string(input_string)

        graph, n = compiled_tasks["concatTask"].implementation().as_graph_viz()
        graph.render()

    # @unittest.skip("Because")
    def test_nonsense_task_def(self):
        input_string = (
            "abcdefasdfhwe >< {\n"
            "\n"
            "}\n"
        )

        print("test_nonsense_task_def: verify parsing nonsense text raises exception on compile")
        print(input_string)
        compiler = TaskCompiler()

        with self.assertRaises(TaskParserException):
            compiler.compile_string(input_string)

    # @unittest.skip("Because")
    def test_invalid_arg_list(self):
        input_string = (
            "Task testTask(a=2 b=3, c) {\n"
            "\n"
            "}\n"
        )

        print("test_invalid_arg_list: verify parsing task with error in arg declaration raises exception on compile")
        print(input_string)
        compiler = TaskCompiler()

        with self.assertRaises(TaskParserException):
            compiler.compile_string(input_string)


class TestNestedTaskParsing(unittest.TestCase):

    # @unittest.skip("Because")
    def test_isolated_nested_task_def(self):
        input_string = (
            "Task convTask() {\n"
            "   Layer Conv(filterSize=3, numFilters=7);\n"
            "}\n"
            "\n"
            "Task mainTask() {\n"
            "   Task convTask();\n"
            "}\n"
        )

        print("test_isolated_nested_task_def: verify parsing a task with a single nested task")
        print(input_string)
        compiler = TaskCompiler()
        compiler.compile_string(input_string)

    # @unittest.skip("Because")
    def test_nested_task_with_sequences_def(self):
        input_string = (
            "Task convTask() {\n"
            "   Layer Conv(filterSize=3, numFilters=7);\n"
            "}\n"
            "\n"
            "Task mainTask(convNumFilters) {\n"
            "   Task convTask();\n"
            "   Layer Conv(filterSize=3, numFilters=convNumFilters);\n"
            "   Layer Conv();\n"
            "   Sequence() {\n"
            "       Layer Conv();\n"
            "       Task convTask();\n"
            "   }\n"
            "}\n"
        )

        print("test_nested_task_with_sequences_def: verify parsing a task with nested tasks, layers and sequences")
        print(input_string)
        compiler = TaskCompiler()
        compiled_tasks = compiler.compile_string(input_string)

        # Uncomment this block if you want to render the the task as a graph.
        graph, n = compiled_tasks["mainTask"].implementation().as_graph_viz()
        graph.render()
