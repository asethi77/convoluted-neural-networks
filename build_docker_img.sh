# Virtualenv attempts to hardcode the path to the python interpreter to ensure all python modules run
# inside the environment at all times. We substitute this with an environment variable lookup, since
# this should result in the same python module being found so long as the user activates the virtual
# environment before running the script.
find ./cnn_env -type f -exec sed -i "s/\#\!.*\/cnn_env\/bin\/python3\.5/\#\!\/usr\/bin\/env python3.5/g" {} +

docker build -t cnn:0.0.0 . -f docker/Dockerfile
