from TaskCompiler.TaskCompiler import TaskCompiler
import tensorflow
from tensorflow.examples.tutorials.mnist import input_data
import tfgraphviz as tfg
import graphviz

mnist = input_data.read_data_sets("MNIST_data/")


def main(batch_size=32):
    input_file = './DemoMNIST.taskdef'

    with tensorflow.Graph().as_default():
        compiler = TaskCompiler()
        compiled_tasks = compiler.compile_file(input_file)
        main_task = compiled_tasks['mainTask'].implementation()
        main_task["input"] = tensorflow.placeholder(tensorflow.float32, shape=(batch_size, 28, 28, 1), name="MainInput")
        output_tensor, _ = main_task.compile()

        main_task_graph, _ = main_task.as_graph_viz()
        main_task_graph.render()

        g = tfg.board(tensorflow.get_default_graph())
        g.view()

        labels_placeholder = tensorflow.placeholder(tensorflow.int32, shape=(batch_size))

        loss = tensorflow.losses.sparse_softmax_cross_entropy(labels=labels_placeholder, logits=output_tensor)
        optimizer = tensorflow.train.GradientDescentOptimizer(0.5)
        global_step = tensorflow.Variable(0, name='global_step', trainable=False)
        train_op = optimizer.minimize(loss, global_step=global_step)

        eval_correct = tensorflow.nn.in_top_k(output_tensor, labels_placeholder, 1)
        eval_num_correct = tensorflow.reduce_sum(tensorflow.cast(eval_correct, tensorflow.int32))

        init = tensorflow.global_variables_initializer()

        sess = tensorflow.Session()
        sess.run(init)
        old_loss_amt = 0.0

        for step in range(1000):
            batch_input, batch_label = mnist.train.next_batch(batch_size)
            batch_input = batch_input.reshape((batch_size, 28, 28, 1))
            feed_dict = {main_task["input"].value: batch_input, labels_placeholder: batch_label}
            _ = sess.run(train_op, feed_dict=feed_dict)
            loss_amt = sess.run(loss, feed_dict=feed_dict)

            if step % 10 == 0:
                print('Step %d: loss = %.2f' % (step, loss_amt))
                print(sess.run(eval_num_correct, feed_dict=feed_dict))

            if abs(loss_amt - old_loss_amt) < 0.01:
                break

            old_loss_amt = loss_amt

        print("\n\nEVALUATING ON TEST SET\n\n")
        batch_num = 0
        for step in range(1000):
            batch_num += 1
            batch_input, batch_label = mnist.test.next_batch(batch_size)
            batch_input = batch_input.reshape((batch_size, 28, 28, 1))
            feed_dict = {main_task["input"].value: batch_input, labels_placeholder: batch_label}
            _ = sess.run(train_op, feed_dict=feed_dict)
            loss_amt = sess.run(loss, feed_dict=feed_dict)
            print("Batch %d: Prediction accuracy is %.2f%%" % (batch_num, sess.run(eval_num_correct, feed_dict=feed_dict) / float(batch_size) * 100.0))


if __name__ == '__main__':
    main()

