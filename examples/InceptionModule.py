from TaskCompiler.TaskCompiler import TaskCompiler
import tensorflow
from tensorflow.examples.tutorials.mnist import input_data
import tfgraphviz as tfg

mnist = input_data.read_data_sets('MNIST_data/')


def main(batch_size=32):
    input_string = (
        'Task convTask() {\n'
        '   Concat() {\n'
        '       Sequence threeByThreeConv() {\n'
        '           Layer Conv(filterSize=1, numFilters=96, padding="SAME", stride=1);\n'
        '           Layer Conv(filterSize=3, numFilters=128, padding="SAME", stride=1);\n'
        '       }\n'
        '       Sequence fiveByFiveConv() {\n'
        '           Layer Conv(filterSize=1, numFilters=16, padding="SAME", stride=1);\n'
        '           Layer Conv(filterSize=5, numFilters=32);\n'
        '       }\n'
        '       Sequence pool() {\n'
        '           Layer MaxPool(poolWindowSize=3);\n'
        '           Layer Conv(filterSize=1, numFilters=32);\n'
        '       }\n'
        '       Layer Conv(filterSize=1, numFilters=64, padding="SAME", stride=1);\n'       # 28x28x64
        '   }\n'
        '}\n'
    )

    print(input_string)

    inception_file = '/home/aakash/PycharmProjects/convoluted-neural-networks/examples/GoogLeNet.taskdef'

    with tensorflow.Graph().as_default():
        compiler = TaskCompiler()
        compiled_tasks = compiler.compile_file(inception_file)
        main_task = compiled_tasks['googlenet'].implementation()
        main_task['input'] = tensorflow.placeholder(tensorflow.float32, shape=(batch_size, 224, 224, 3), name='MainInput')
        output_tensor, _ = main_task.compile()

        # labels_placeholder = tensorflow.placeholder(tensorflow.int32, shape=(batch_size))
        #
        # loss = tensorflow.losses.sparse_softmax_cross_entropy(labels=labels_placeholder, logits=output_tensor)
        # optimizer = tensorflow.train.GradientDescentOptimizer(0.5)
        # global_step = tensorflow.Variable(0, name='global_step', trainable=False)
        # train_op = optimizer.minimize(loss, global_step=global_step)
        #
        # eval_correct = tensorflow.nn.in_top_k(output_tensor, labels_placeholder, 1)
        # eval_num_correct = tensorflow.reduce_sum(tensorflow.cast(eval_correct, tensorflow.int32))
        #
        # summary = tensorflow.summary.merge_all()
        #
        # init = tensorflow.global_variables_initializer()

        g = tfg.board(tensorflow.get_default_graph())
        g.view()


if __name__ == '__main__':
    main()
