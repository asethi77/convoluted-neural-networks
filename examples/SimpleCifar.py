from keras.datasets import cifar10
from TaskCompiler.TaskCompiler import TaskCompiler
import tensorflow
import tfgraphviz as tfg

(X_train, y_train), (X_test, y_test) = cifar10.load_data()
X_train = X_train.astype('float32')
X_test = X_test.astype('float32')
X_train = X_train / 255.0
X_test = X_test / 255.0

def fake_main():
    print(y_train.min(0))
    print(y_train.max(0))

def main(batch_size=32):
    input_file = '/home/aakash/PycharmProjects/convoluted-neural-networks/examples/SimpleCifar.taskdef'

    with tensorflow.Graph().as_default():
        compiler = TaskCompiler()
        compiled_tasks = compiler.compile_file(input_file)
        main_task = compiled_tasks['Cifar'].implementation()
        main_task["input"] = tensorflow.placeholder(tensorflow.float32, shape=(batch_size, 32, 32, 3), name="MainInput")
        output_tensor, _ = main_task.compile()

        main_task_graph, _ = main_task.as_graph_viz()
        main_task_graph.render()

        labels_placeholder = tensorflow.placeholder(tensorflow.int32, shape=(batch_size))

        loss = tensorflow.losses.sparse_softmax_cross_entropy(labels=labels_placeholder, logits=output_tensor)
        optimizer = tensorflow.train.GradientDescentOptimizer(0.5)
        global_step = tensorflow.Variable(0, name='global_step', trainable=False)
        train_op = optimizer.minimize(loss, global_step=global_step)

        eval_correct = tensorflow.nn.in_top_k(output_tensor, labels_placeholder, 1)
        eval_num_correct = tensorflow.reduce_sum(tensorflow.cast(eval_correct, tensorflow.int32))

        init = tensorflow.global_variables_initializer()

        sess = tensorflow.Session()
        sess.run(init)
        old_loss_amt = 0.0

        batch_num = 0
        num_batches = int(X_train.shape[0] / batch_size)
        for step in range(num_batches):
            batch_input = X_train[batch_num * batch_size: (batch_num + 1) * batch_size]
            batch_label = y_train[batch_num * batch_size: (batch_num + 1) * batch_size]
            batch_label = batch_label.reshape((32,))
            batch_input = batch_input.reshape((batch_size, 32, 32, 3))
            feed_dict = {main_task["input"].value: batch_input, labels_placeholder: batch_label}
            _ = sess.run(train_op, feed_dict=feed_dict)
            loss_amt = sess.run(loss, feed_dict=feed_dict)

            print('Step %d: loss = %.2f' % (step, loss_amt))
            print(sess.run(eval_num_correct, feed_dict=feed_dict))

            if abs(loss_amt - old_loss_amt) < 0.01:
                break

            old_loss_amt = loss_amt
            batch_num += 1

        print("\n\nEVALUATING ON TEST SET\n\n")
        batch_num = 0
        num_batches = int(X_test.shape[0] / batch_size)
        for step in range(num_batches - 1):
            batch_num += 1
            batch_input = X_test[batch_num * batch_size: (batch_num + 1) * batch_size]
            batch_label = y_test[batch_num * batch_size: (batch_num + 1) * batch_size]
            batch_input = batch_input.reshape((batch_size, 32, 32, 3))
            batch_label = batch_label.reshape((32,))
            feed_dict = {main_task["input"].value: batch_input, labels_placeholder: batch_label}
            _ = sess.run(train_op, feed_dict=feed_dict)
            loss_amt = sess.run(loss, feed_dict=feed_dict)
            print("Batch %d: Prediction accuracy is %.2f%%" % (batch_num, sess.run(eval_num_correct, feed_dict=feed_dict) / float(batch_size) * 100.0))

        g = tfg.board(tensorflow.get_default_graph())
        g.view()


if __name__ == '__main__':
    main()
